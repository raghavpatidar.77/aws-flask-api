
import get_file as gf
import psutil


#  print(psutil.cpu_times())
def output():
    data  =  gf.get_file()
    result = {}
    pages = {}
    pages_data = []
    TEXT = 'True'
    confidence = "False"
    bob = "False"
    depth = "word"

    data_len = len(data["Blocks"])
    depth = depth.upper()
    page_indx = []
    line_indx = []
    word_indx = []

    lines_data = []

    word_data = []
    start_word = -1

    # print(data.keys())

    for x in range(0,data_len) :
        if depth == "PAGE" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
        elif depth == "LINE" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "LINE" :
                line_indx.append(x)
        elif depth == "WORD" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "LINE" :
                line_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "WORD" :
                word_indx.append(x)
                
    page_len = len(page_indx)
    line_len = len(line_indx)
    word_len = len(word_indx)
    print(page_len)
    print(line_len)
    print(word_len)
    if page_len>0 :
        for itr in page_indx :
            
            lines = {}
            lines["page"] = data["Blocks"][itr]["Page"]
            poly = data["Blocks"][itr]["Geometry"]["Polygon"]
            lines["Polygon"] = [poly[0],poly[2]]
            
            if line_len>0 :
                for line_ind in line_indx :
                    
                    new_d = {}
                    word_ids = []
                    # new_d["text"] = data["Blocks"][line_ind]["Text"]
                    
                    if data["Blocks"][itr]["Page"] == data["Blocks"][line_ind]["Page"] :
                        
                        if TEXT == "True" :
                            
                            # new_d["Page"] = data["Blocks"][x]["Page"]
                            new_d["text"] = data["Blocks"][line_ind]["Text"]
                            
                            if confidence == "True" :
                                
                                new_d["confidence"] = data["Blocks"][line_ind]["Confidence"]
                                
                                if bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                            elif bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                        elif confidence == "True" :
                            new_d["confidence"] = data["Blocks"][line_ind]["Confidence"]
                            if bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                        elif bob == "True" :
                            
                            poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                            new_d["Polygon"] = [poly[0],poly[2]]
                            
                        if word_len>0 :
                            
                            word_ids = data["Blocks"][line_ind]["Relationships"][0]["Ids"]
                            word_data = []
                            
                            for ids in word_ids :
                                
                                start_word +=  1
                                word = {}
                                
                                if data["Blocks"][word_indx[start_word]]["Id"] == ids :
                                    
                                    
                                    # word["text"] = data["Blocks"][word_indx[start_word]]["Text"]
                                    
                                    if TEXT == "True" :
                                        
                                        # new_d["Page"] = data["Blocks"][x]["Page"]
                                        
                                        word["text"] = data["Blocks"][word_indx[start_word]]["Text"]
                                        
                                        if confidence == "True":
                                            
                                            word["confidence"] = data["Blocks"][word_indx[start_word]]["Confidence"]
                                            
                                            if bob == "True" :
                                                
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                        elif bob == "True" :
                                                
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                    elif confidence == "True" :
                                        
                                        word["confidence"] = data["Blocks"][word_indx[start_word]]["Confidence"]
                                        
                                        if bob == "True" :
                                            
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                    elif bob == "True" :
                                        
                                        poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                        word["Polygon"] = [poly[0],poly[2]]
                                
                                word_data.append(word)
                            new_d["word"] = word_data
                        if len(new_d)>0:    
                            lines_data.append(new_d)
                    else :
                        continue
            if len(lines_data)> 0 :
                lines["lines"] = lines_data
            pages_data.append(lines)


    pages["pages"] = pages_data
    result["response"] = pages
        
    print(result)
    # print(psutil.cpu_times())
    # {"response" :{"pages" :[{"page" :1,"lines" :[{"text","word"}]}]},{"page" :2}}

output()