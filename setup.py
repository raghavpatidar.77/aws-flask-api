from setuptools import setup ,find_packages

setup(
    name="AWS_Textract",
    version=1.0,
    author_email="patidar.raghav@tcs.com",
    description="AWS textract Flask API",
    package_dir={"":"app"},
    packages=find_packages(where="AWS_PRACTICE")
    
)

