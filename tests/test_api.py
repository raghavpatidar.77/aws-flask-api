import unittest
import os
import sys
import json
from AWS_textract.main import app
 

# C:\Users\user\Flask_app\tests\input_data
with open(r"{path}\tests\input_data\test_sync.json".format(path=os.getcwd())) as f:
    sync_data=json.load(f)

with open(r"{path}\tests\input_data\test_async.json".format(path=os.getcwd())) as f:
    async_data=json.load(f)

with open(r"{path}\tests\input_data\test_operation_status.json".format(path=os.getcwd())) as f:
    operation_data=json.load(f)

class AWSTestCase(unittest.TestCase):
    """This class represents AWS test cases"""

    def test_sync(self):
        for key in sync_data:
            tester=app.test_client(self)
            res = tester.post('/Sync', json=sync_data[key]["request"])
            self.assertEqual(res.status_code, sync_data[key]["response_code"])
        
    # Testcase-2 to check the response if wrong destination-uri is passed
    def test_async(self):
        for key in async_data:
            tester=app.test_client(self)
            res = tester.post('/Async',  json=async_data[key]["request"])
            self.assertEqual(res.status_code,  async_data[key]["response_code"])
        
    # TestCase-3 tp check the response if wrong file uri is passed
    def test_operation(self):
        for key in operation_data:
            tester=app.test_client(self)
            res = tester.post('/operation_status', json=operation_data[key]["request"])
            self.assertEqual(res.status_code, operation_data[key]["response_code"])
        
"""if __name__ == "__main__":
    unittest.main()"""