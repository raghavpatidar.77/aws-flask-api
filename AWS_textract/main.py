from flask import Flask,request,jsonify,Response
from utlis import *
import logging

app =  Flask(__name__)
logging.basicConfig(filename =  'record.log', level =  logging.ERROR, format =  f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s',filemode =  'w')
bucket_name =  ''
file_name =  ''

@app.route('/health_check')
def check_health():
    return jsonify({"message":"server is working"}),200

@app.route('/Sync',methods =  ['POST'])
def detect_text(): 
    try:
        app.logger.info('Started detecting')
        app.logger.warning('Warning level log')
        if request.method == 'POST':
            
            #reading the input 
            data =  request.json
            
            #checking the input format
            check =  validate_sync(data)
            #check =  True
            
            #print(data)
            if check == True:
                app.logger.info('Input Validation succedded')
                if data["Data"]["vendor_name"] == "aws":
                    
                    #reading s3-uri,bucket_name ,flags ,output-uri and all
                    s3_url =  (data["Data"]["s3_file_uri"]).split("/")
                    bucket_name =  s3_url[2]
                    file_name =  s3_url[3]
                    output_url =  (data["Data"]["destination_uri"]).split("//")
                    output_bucket =  output_url[1]
                    
                    text_flag =  data["Data"]["create_file_with_text_only"]
                    if text_flag == "False":
                        text =  data["Data"]["include_ocr_text"]
                        confidence =  data["Data"]["include_ocr_confidence"]
                        bob =  data["Data"]["include_ocr_boundingbox"]
                        
                    else:
                        text =  "False"
                        confidence =  "False"
                        bob =  "False"
                    
                    depth =  data["Data"]["ocr_depth"]
                    #checking if input file exist or not
                    res =  check_file(bucket_name,file_name)
                    
                    
                    if res == True:
                        app.logger.info(' input file existed : ')
                        
                        #checking if output bucket exist or not
                        res2 =  check_bucket(output_bucket)
                        
                        
                        if res2 == True:
                        #cf.getfilename(file_name)
                            app.logger.info("Output bucket exist")
                            #uploading the file name to get output file name
                            set_file_name(file_name)
                            app.logger.info('uploading the file name')
                            
                            #getting the result from text detection
                            result =  (detect_text_document(bucket_name,file_name))
                            app.logger.info('getting the result from aws')
                            
                            ext_out =  output(result,text,confidence,bob,depth)
                            
                            print(ext_out)
                            if text_flag == "False":
                                upload(ext_out,output_bucket,text_flag)
                            #uploading the output file to output bucket
                            file_uri =  upload(result,output_bucket,text_flag)
                            
                            app.logger.info('uploading the output file to output bucket')
                            return jsonify({"message":"Operation Successfull","response_code":result["ResponseMetadata"]["HTTPStatusCode"],"output_File_uri":file_uri})
                            #return jsonify({"response": {"response":result,"message":"Document Detected","response_code":str(Response())}})
                        else:
                            app.logger.error("Output bucket does not exsits")
                            return jsonify({"Error ": res2[0]}),res2[1]
                            
                    else:
                        app.logger.error("Input file does not exsits")
                        return jsonify({"Error ": res[0]}),res[1]
                        
            else:
                app.logger.error('Input Validation failed')
                return jsonify({ "error": str(check) ,"response_code":Response()}),404
               
                #print(result[0])
                #result_text =  aws_td.getResults(result[0],result[1])
    except Exception as e:
        app.logger.error('Unknown Error Occured')
        #print(e)
        return jsonify({"error":str(e),"problem":"Error in Uploading file"}),500
        
    
    #return jsonify({"Data":[bucket_name,file_name]})

@app.route('/Async',methods =  ['POST'])
def text_detection():
    try:
        if request.method == 'POST':
            data =  request.json
            check =  validate(data)
            #print(check)
            if check == True:
                if data["Data"]["vendor_name"] == "aws":
                    s3_url =  (data["Data"]["s3_file_uri"]).split("/")
                    bucket_name =  s3_url[2]
                    file_name =  s3_url[3]
                    res =  check_file(bucket_name,file_name)
                    if res == True:
                    #cf.getfilename(file_name)
                        set_file_name(file_name)
                        result =  (Invoke_text_detection(bucket_name,file_name))
                        return jsonify({"response": {"operation_id":result,"message":"Document Detection started","response_code":str(Response())}})
                    else:
                        return jsonify({"Error ": res[0]}),res[1]
            else:
                return jsonify({ "error": str(check) ,"response_code":Response()}),404
                #print(result[0])
                #result_text =  aws_td.getResults(result[0],result[1])
    except Exception as e:
        return jsonify({"error":str(e),"response_code":str(Response())}),400
            

@app.route("/operation_status",methods =  ["POST"])
def getResult():
    try:
        if request.method == "POST":
            data =  request.json
            check =  validate_get(data)
            #print(check)
            if check == True:
                jobid =  data["Data"]["operation_id"]
                output_url =  (data["Data"]["destination_uri"]).split("//")
                output_bucket =  output_url[1]
                res =  check_bucket(output_bucket)
                if res == True:
                    #print(text,confidence,bob)
                    ##cf.create(data,text,confidence,bob)
                    text_flag =  data["Data"]["create_file_with_text_only"]
                    
                    #input_fields =  data["Data"]["input_flags"].split(",")

                    max_retries =  data["Data"]["max_retries"]
                    frequency =  data["Data"]["frequency"]
                    text =  data["Data"]["include_ocr_text"]
                    confidence =  data["Data"]["include_ocr_confidence"]
                    bob =  data["Data"]["include_ocr_boundingbox"]
                    depth =  data["Data"]["ocr_depth"]
                    
                    result =   getResults(jobid,max_retries,frequency)
                    if result == "IN_PROGRESS":
                        return jsonify({"Operation_status":result}),202
                    else:
                        if text_flag == "False":
                            read_response =  (output(result,text,confidence,bob,depth))
                            flags_uri =  upload(read_response,output_bucket+"/flag_output","False")
                            file_uri =  upload(result,output_bucket+"/output",text_flag)
                            return jsonify({"message":"Operation Successfull","response_code":result["ResponseMetadata"]["HTTPStatusCode"],"output_File_uri":file_uri,"flags_output_file_uri":flags_uri})
                        else:
                            file_uri =  upload(result,output_bucket,text_flag)
                            return jsonify({"message":"Operation Successfull","response_code":result["ResponseMetadata"]["HTTPStatusCode"],"output_File_uri":file_uri})
                        #s3://sampleinput2/text_detection_document.pdf
                    ##cf.create(result,output_bucket)
                    
                else:
                    return jsonify({"Error ": res[0]}),res[1]
                
            else:
                return jsonify({"Error":str(check)}),404
    # except  Exception as e:
        # return jsonify({"Error":str(e.response['Error']['Message'])}),str (e.response['ResponseMetadata']['HTTPStatusCode'])
        
    except Exception as e:
        return jsonify({"error":str(e),"Type":" Unknown Error"}),400 
 

if __name__ == '__main__': 
    app.run(port =  5000,debug =  True,host =  "0.0.0.0")