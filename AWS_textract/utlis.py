import boto3
import time
import os
import json
import smart_open
import datetime as dt
from jsonschema import Draft4Validator as d4v


# Sync Operation
def detect_text_document(bucket_name,file_name):
    client=boto3.client('textract')
    response = client.detect_document_text(
        Document={
            'S3Object': {
                'Bucket': bucket_name,
                'Name': file_name
                } 
            }
    )
    return response

# Aysnc Operation
def Invoke_text_detection(bucket_name,file_name):
    client=boto3.client('textract')

    response = client.start_document_text_detection(
        DocumentLocation={
            'S3Object': {
                'Bucket': bucket_name,
                'Name': file_name
                
            }
        }
    )
    
    JobID=(response["JobId"])
    
    return JobID
    
def checkJobStatus(JobID,max_retries,frequency):
    i=0
    client=boto3.client('textract')
    response=client.get_document_text_detection(JobId=JobID )
    status="IN_PROGRESS"
    #print("Job Status : {}".format(status),"frequency :",frequency,"max_retry :",max_retries)
    while (i<max_retries):
        i+=1
        time.sleep(frequency)
        response=client.get_document_text_detection(JobId=JobID)
        status=response["JobStatus"]
        if status=="SUCCEEDED":
            break
        
        #print("Job Status : {}".format(status))
    return status

def getResults(JobID,max_retries,frequency):
    Job_status=(checkJobStatus(JobID,max_retries,frequency))
    if Job_status=="SUCCEEDED": 
        client=boto3.client('textract')
        response=client.get_document_text_detection(JobId=JobID)
        while(1):
            if "NextToken" in response.keys():
                result=[]
                next_token=response["NextToken"]
                result=client.get_document_text_detection(JobId=JobID,NextToken=next_token)
                response["Blocks"].extend(result["Blocks"])
                if "NextToken" in result.keys():
                    response["NextToken"]=result["NextToken"]
                else:
                    break
            else:
                break
        #print(response)
        return response
    else:
        return Job_status

#detect_text("sampleinput2","sampleText2.png")




#with open(r"C:\Users\user\Flask App Project\AWS-practice\create_output_files\Acknowledgement_Slip.pdf.json") as f:
    #data=json.load(f)
    
def get_text(data):
        result={}

        l=len(data["Blocks"])
        #print(data["Blocks"][0]["BlockType"])
        #return(data["Blocks"])
        text=''
        
        for x in range(1,l):
            if "Text" in data["Blocks"][x].keys():
                if data["Blocks"][x]["BlockType"]=="LINE":
                        text+=data["Blocks"][x]["Text"]     
                text+=" "
                #print(text)
                
                
    
        result["Extracted text"]=text
            
        return str(result)
 
 


file_names=[]


def upload(data,bucket_name,text_flag):
    ct=dt.datetime.now()
    file_name=get_file_name().split(".")
    output_file_path='s3://{bucket}/{timest}/{name}.json'.format(bucket=bucket_name,timest=ct.timestamp(),name=file_name[0])
    #print("="*100,"\nResult : ",str(ro.read(data,flags)))
    #print(text_flag)
    
    if text_flag=="True":
          
        #print("="*100,"\nResult : ",str(ro.get_text(data)))
        with smart_open.smart_open(output_file_path, 'w') as fout:
            reads=get_text(data)
            fout.write(str(reads))
            
            
    else:
        
        #print(data)
        with smart_open.smart_open(output_file_path, 'w') as fout:
            fout.write(str(data))
        
    return output_file_path

def set_file_name(name):
    file_names.append(name)

        
def get_file_name():
    if len(file_names)>0:
        return file_names.pop()
    else:
        return "default_name"






def output(data,TEXT,confidence,bob,depth):
    begin=time.time()
    result = {}
    pages = {}
    pages_data = []
    
    # TEXT = 'True'
    # confidence = "False"
    # bob = "True"
    # depth = "word"

    data_len = len(data["Blocks"])
    depth = depth.upper()
    page_indx = []
    line_indx = []
    word_indx = []

    lines_data = []

    word_data = []
    start_word = -1

    # print(data.keys())

    for x in range(0,data_len) :
        if depth == "PAGE" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
        elif depth == "LINE" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "LINE" :
                line_indx.append(x)
        elif depth == "WORD" :
            if data["Blocks"][x]["BlockType"] == "PAGE" :
                page_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "LINE" :
                line_indx.append(x)
            elif data["Blocks"][x]["BlockType"] == "WORD" :
                word_indx.append(x)
                
    page_len = len(page_indx)
    line_len = len(line_indx)
    word_len = len(word_indx)
    
    # print(page_len)
    # print(line_len)
    # print(word_len)
    
    if page_len>0 :
        for itr in page_indx :
            
            lines = {}
            lines["page"] = data["Blocks"][itr]["Page"]
            poly = data["Blocks"][itr]["Geometry"]["Polygon"]
            lines["Polygon"] = [poly[0],poly[2]]
            
            if line_len>0 :
                for line_ind in line_indx :
                    
                    new_d = {}
                    word_ids = []
                    # new_d["text"] = data["Blocks"][line_ind]["Text"]
                    
                    if data["Blocks"][itr]["Page"] == data["Blocks"][line_ind]["Page"] :
                        
                        if TEXT == "True" :
                            
                            # new_d["Page"] = data["Blocks"][x]["Page"]
                            new_d["text"] = data["Blocks"][line_ind]["Text"]
                            
                            if confidence == "True" :
                                
                                new_d["confidence"] = data["Blocks"][line_ind]["Confidence"]
                                
                                if bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                            elif bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                        elif confidence == "True" :
                            new_d["confidence"] = data["Blocks"][line_ind]["Confidence"]
                            if bob == "True" :
                                    
                                    poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                                    new_d["Polygon"] = [poly[0],poly[2]]
                                    
                        elif bob == "True" :
                            
                            poly = data["Blocks"][line_ind]["Geometry"]["Polygon"]
                            new_d["Polygon"] = [poly[0],poly[2]]
                            
                        if word_len>0 :
                            
                            word_ids = data["Blocks"][line_ind]["Relationships"][0]["Ids"]
                            word_data = []
                            
                            for ids in word_ids :
                                
                                start_word +=  1
                                word = {}
                                
                                if data["Blocks"][word_indx[start_word]]["Id"] == ids :
                                    
                                    
                                    # word["text"] = data["Blocks"][word_indx[start_word]]["Text"]
                                    
                                    if TEXT == "True" :
                                        
                                        # new_d["Page"] = data["Blocks"][x]["Page"]
                                        
                                        word["text"] = data["Blocks"][word_indx[start_word]]["Text"]
                                        
                                        if confidence == "True":
                                            
                                            word["confidence"] = data["Blocks"][word_indx[start_word]]["Confidence"]
                                            
                                            if bob == "True" :
                                                
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                        elif bob == "True" :
                                                
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                    elif confidence == "True" :
                                        
                                        word["confidence"] = data["Blocks"][word_indx[start_word]]["Confidence"]
                                        
                                        if bob == "True" :
                                            
                                                poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                                word["Polygon"] = [poly[0],poly[2]]
                                                
                                    elif bob == "True" :
                                        
                                        poly = data["Blocks"][word_indx[start_word]]["Geometry"]["Polygon"]
                                        word["Polygon"] = [poly[0],poly[2]]
                                
                                word_data.append(word)
                            new_d["word"] = word_data
                        if len(new_d)>0:    
                            lines_data.append(new_d)
                    else :
                        continue
            if len(lines_data)> 0 :
                lines["lines"] = lines_data
            pages_data.append(lines)


    pages["pages"] = pages_data
    result["response"] = pages
    
    end=time.time()
    print("begin_time : ",begin,"\n","end_time :",end)
    print("time_taken : ",end-begin)
    return str(result)

    # print(psutil.cpu_times())
    # {"response" :{"pages" :[{"page" :1,"lines" :[{"text","word"}]}]},{"page" :2}}

# C:\Users\user\Flask_app\AWS_textract\data\schema.json
def validate(check):
    path=r"{path}\AWS_textract\data\schema.json".format(path=os.getcwd())

    with open(path) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid
        

def validate_get(check):
    pathn=r"{path}\AWS_textract\data\get_schema.json".format(path=os.getcwd())
    with open(pathn) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid
        
def validate_sync(check):
    paths=r"{path}\AWS_textract\data\sync_schema.json".format(path=os.getcwd())
    with open(paths) as f:
        schema=json.load(f)
        validator=d4v(schema)
        valid=list(validator.iter_errors(check))
        #print(list(validator.iter_errors(check,schema)))
        if valid ==[]:
            return True
        else:
            return valid




def check_bucket(bucket_name):
    try:
        if "/" in bucket_name :
            name = bucket_name.split("/")
            bucket_name = name[0]
        client=boto3.resource("s3")
        client.meta.client.head_bucket(Bucket=bucket_name)
        return True
    
    except Exception as e:
        return ["File/Bucket "+str(e.response['Error']['Message']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
       
        #error= "Bucket does not exist "
        
        #return [error,404]
        
#print(check_bucket("sampleinput2/"))

def check_file(bucket_name,file_name):
    s3 = boto3.resource('s3')
    try:
        s3.Object(bucket_name,file_name).load()
        return True
        
    except Exception as e:
        return ["File/Bucket "+str(e.response['Error']['Message']),str(e.response['ResponseMetadata']['HTTPStatusCode'])]
        #return ["Unknown Error \n",400]
    
#print(check_file("sampleinput2","Programming,ADA Using Python.pdf"))